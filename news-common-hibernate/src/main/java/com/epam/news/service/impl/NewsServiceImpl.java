package com.epam.news.service.impl;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.INewsService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * Class provides operations with news
 */
@Service
public class NewsServiceImpl implements INewsService{

    private static Logger logger= Logger.getLogger(NewsServiceImpl.class);
    private INewsDAO newsDAO;
    /**
     * Finds news by id
     * @param id
     * @return news
     * @throws ServiceException
     */
    @Override
    public News findById(Long id) throws ServiceException {
        News result = null;
        try{
            result = newsDAO.findById(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Deletes news by id
     * @param id of news
     * @throws ServiceException
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try{
            newsDAO.delete(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Creates news with given info
     * @param entity
     * @throws ServiceException
     */
    @Override
    public void add(News entity) throws ServiceException {
        try{
            entity.setId(newsDAO.add(entity));
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Updates news info by id
     * @param entity
     * @throws ServiceException
     */
    @Override
    public void update(News entity) throws ServiceException {
        try{
            newsDAO.update(entity);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Finds count of news by given tags, author, tags, page
     * @param filterVO
     * @return count of news
     * @throws ServiceException
     */
    @Override
    public int countNewsByFilter(FilterVO filterVO) throws ServiceException {
        int result = 0;
        try{
            result = newsDAO.countNewsByFilter(filterVO);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Finds news number in list of filtered news
     * @param newsId
     * @return news number
     * @throws ServiceException
     */
    @Override
    public Integer newsNumberFromFilter(Long newsId, FilterVO filterVO) throws ServiceException {
        Integer result = null;
        try{
            result = newsDAO.newsNumberFromFilter(newsId,filterVO);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Finds news from filter by news number
     * @param newsNumber
     * @param filterVO
     * @return news
     * @throws ServiceException
     */
    @Override
    public News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws ServiceException {
        News result = null;
        try{
            result = newsDAO.newsFromFilterByNumber(newsNumber,filterVO);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Returns list of news from filter on given page number.
     * Page numbers begin with 1. News ordered by comments number and modification date
     * @param newsOnPage how many news will be on page
     * @param pageNumber determine number of page which should be return
     * @param filterVO
     * @return list of news on given page
     */
    @Override
    public List<News> findNewsByFilters(int newsOnPage, int pageNumber, FilterVO filterVO) throws ServiceException {
        List<News> result = null;
        try{
            result = newsDAO.findNewsByFilters(newsOnPage,pageNumber,filterVO);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    @Override
    public void setNewsDAO(INewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }
}
