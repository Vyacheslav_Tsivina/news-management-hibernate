package com.epam.news.service.impl;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * Class provides operations with tags
 *
 */
@Service
public class TagServiceImpl implements ITagService {

    static private Logger logger= Logger.getLogger(TagServiceImpl.class);
    private ITagDAO tagDAO;
    /**
     * Finds all tags
     * @return List of tags
     * @throws ServiceException
     */
    @Override
    public List<Tag> findAll() throws ServiceException {
        List<Tag> result = null;
        try{
            result = tagDAO.findAll();
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Finds tag by id
     * @param id
     * @return tag if find is successful, returns null otherwise
     * @throws ServiceException
     */
    @Override
    public Tag findById(Long id) throws ServiceException {
        Tag result = null;
        try{
            result = tagDAO.findById(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Deletes tag by id
     * @param id of tag
     * @throws ServiceException
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try{
            tagDAO.delete(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Creates tag with given info
     * @param entity
     * @throws ServiceException
     */
    @Override
    public void add(Tag entity) throws ServiceException {
        try{
            entity.setId(tagDAO.add(entity));
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Updates tag info by id
     * @param entity
     * @throws ServiceException
     */
    @Override
    public void update(Tag entity) throws ServiceException {
        try{
            tagDAO.update(entity);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    @Override
    public void setTagDAO(ITagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }
}
