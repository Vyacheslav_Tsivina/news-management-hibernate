package com.epam.news.service;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;

import java.util.List;

public interface IAuthorService {
    /**
     * Finds all authors
     * @return List of authors
     * @throws ServiceException
     */
    List<Author> findAll() throws ServiceException;
    /**
     * Finds author by id
     * @param id
     * @throws ServiceException
     */
    Author findById(Long id) throws ServiceException;
    /**
     * Deletes author by id
     * @param id of author
     * @throws ServiceException
     */
    void delete(Long id) throws ServiceException;
    /**
     * Creates author with given info
     * @param entity
     * @throws ServiceException
     */
    void add(Author entity) throws ServiceException;
    /**
     * Updates author info by id
     * @param entity
     * @throws ServiceException
     */
    void update(Author entity) throws ServiceException;

    public void setAuthorDAO(IAuthorDAO authorDAO);
}
