package com.epam.news.service;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.exception.ServiceException;

import java.util.List;

public interface INewsService {
    /**
     * Finds news by id
     * @param id
     * @return news
     * @throws ServiceException
     */
    News findById(Long id) throws ServiceException;
    /**
     * Deletes news by id
     * @param id of news
     * @throws ServiceException
     */
    void delete(Long id) throws ServiceException;
    /**
     * Creates news with given info
     * @param entity
     * @throws ServiceException
     */
    void add(News entity) throws ServiceException;
    /**
     * Updates news info by id
     * @param entity
     * @throws ServiceException
     */
    void update(News entity) throws ServiceException;

    /**
     * Finds count of news by given tags, author, tags, page
     * @param filterVO
     * @return count of news
     * @throws ServiceException
     */
    int countNewsByFilter(FilterVO filterVO) throws ServiceException;

    /**
     * Finds news number in list of filtered news
     * @param newsId
     * @return news number
     * @throws ServiceException
     */
    Integer newsNumberFromFilter(Long newsId, FilterVO filterVO) throws ServiceException;

    /**
     * Finds news from filter by news number
     * @param newsNumber
     * @param filterVO
     * @return news
     * @throws ServiceException
     */
    News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws ServiceException;
    /**
     * Returns list of news from filter on given page number.
     * Page numbers begin with 1. News ordered by comments number and modification date
     * @param newsOnPage how many news will be on page
     * @param pageNumber determine number of page which should be return
     * @param filterVO
     * @return list of news on given page
     */
    public List<News> findNewsByFilters( int newsOnPage, int pageNumber, FilterVO filterVO) throws ServiceException;

    void setNewsDAO(INewsDAO newsDAO);
}
