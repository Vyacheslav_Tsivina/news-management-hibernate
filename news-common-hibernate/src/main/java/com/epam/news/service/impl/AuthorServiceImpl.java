package com.epam.news.service.impl;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Class provides operations with authors
 */
@Service
public class AuthorServiceImpl implements IAuthorService{

    private static Logger logger= Logger.getLogger(AuthorServiceImpl.class);
    private IAuthorDAO authorDAO ;

    /**
     * Finds all authors
     * @return List of authors
     * @throws ServiceException
     */
    @Override
    public List<Author> findAll() throws ServiceException {
        List<Author> resultList = null;
        try{
            resultList = authorDAO.findAll();
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return resultList;
    }
    /**
     * Finds author by id
     * @param id
     * @throws ServiceException
     */
    @Override
    public Author findById(Long id) throws ServiceException {
        Author result = null;
        try{
            result = authorDAO.findById(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Deletes author by id
     * @param id of author
     * @throws ServiceException
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try{
            authorDAO.delete(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Adds new author and set inserted id into author
     * @param entity to be added
     */
    @Override
    public void add(Author entity) throws ServiceException {
        try{
            entity.setId(authorDAO.add(entity));
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Updates author info by id
     * @param entity
     * @throws ServiceException
     */
    @Override
    public void update(Author entity) throws ServiceException {
        try{
            authorDAO.update(entity);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    @Override
    public void setAuthorDAO(IAuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }
}
