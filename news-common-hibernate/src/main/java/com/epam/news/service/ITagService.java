package com.epam.news.service;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

import java.util.List;

public interface ITagService {
    /**
     * Finds all tags
     * @return List of tags
     * @throws ServiceException
     */
    List<Tag> findAll() throws ServiceException;
    /**
     * Finds tag by id
     * @param id
     * @return tag if find is successful, returns null otherwise
     * @throws ServiceException
     */
    Tag findById(Long id) throws ServiceException;
    /**
     * Deletes tag by id
     * @param id of tag
     * @throws ServiceException
     */
    void delete(Long id) throws ServiceException;
    /**
     * Creates tag with given info
     * @param entity
     * @throws ServiceException
     */
    void add(Tag entity) throws ServiceException;
    /**
     * Updates tag info by id
     * @param entity
     * @throws ServiceException
     */
    void update(Tag entity) throws ServiceException;

    void setTagDAO(ITagDAO tagDAO);
}
