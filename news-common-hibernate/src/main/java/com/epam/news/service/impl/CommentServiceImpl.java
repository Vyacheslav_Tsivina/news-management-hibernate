package com.epam.news.service.impl;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ICommentService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * Class provides operations with comments
 */
@Service
public class CommentServiceImpl implements ICommentService {

    private static Logger logger= Logger.getLogger(CommentServiceImpl.class);
    private ICommentDAO commentDAO;

    /**
     * Finds comment by id
     * @param id
     * @return comment if find is successful, returns null otherwise
     * @throws ServiceException
     */
    @Override
    public Comment findById(Long id) throws ServiceException {
        Comment result = null;
        try{
            result = commentDAO.findById(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Deletes comment by id
     * @param id of comment
     * @throws ServiceException
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try{
            commentDAO.delete(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Creates comment with given info
     * @param entity
     * @throws ServiceException
     */
    @Override
    public void add(Comment entity) throws ServiceException {
        try{
            entity.setId(commentDAO.add(entity));
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Updates comment info by id
     * @param entity
     * @throws ServiceException
     */
    @Override
    public void update(Comment entity) throws ServiceException {
        try{
            commentDAO.update(entity);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    @Override
    public void setCommentDAO(ICommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }
}
