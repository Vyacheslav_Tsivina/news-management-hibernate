package com.epam.news.service;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.ServiceException;

public interface ICommentService {
    /**
     * Finds comment by id
     * @param id
     * @return comment if find is successful, returns null otherwise
     * @throws ServiceException
     */
    Comment findById(Long id) throws ServiceException;
    /**
     * Deletes comment by id
     * @param id of comment
     * @throws ServiceException
     */
    void delete(Long id) throws ServiceException;
    /**
     * Creates comment with given info
     * @param entity
     * @throws ServiceException
     */
    void add(Comment entity) throws ServiceException;
    /**
     * Updates comment info by id
     * @param entity
     * @throws ServiceException
     */
    void update(Comment entity) throws ServiceException;

    void setCommentDAO(ICommentDAO commentDAO);
}
