package com.epam.news.dao.jpa;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.util.JPAEntityManager;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.JPADAOUtil.*;

public class JPAAuthorDAO implements IAuthorDAO {

    private EntityManagerFactory managerFactory;
    public JPAAuthorDAO(){
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }
    /**
     * Finds all authors
     * @return List of authors
     * @throws DAOException
     */
    @Override
    public List<Author> findAll() throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction ;
        List<Author> result = new ArrayList<>();
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT a FROM Author a ORDER BY a.name");
            result = query.getResultList();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Finds author by id
     * @param id
     * @throws DAOException
     */
    @Override
    public Author findById(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;
        Author result = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            result = manager.find(Author.class, id);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Deletes author by id
     * @param id of author
     * @throws DAOException
     */
    @Override
    public void delete(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;

        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Author entity = manager.find(Author.class,id);
            manager.remove(entity);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }
    /**
     * Creates author with given info
     * @param entity
     * @return id of inserted author
     * @throws DAOException
     */
    @Override
    public Long add(Author entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            entity.setId(null);
            manager.persist(entity);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return entity.getId();
    }
    /**
     * Updates author info by id
     * @param entity
     * @throws DAOException
     */
    @Override
    public void update(Author entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Author tempAuthor = manager.find(Author.class,entity.getId());
            tempAuthor.setName(entity.getName());
            tempAuthor.setExpired(entity.getExpired());
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }


}
