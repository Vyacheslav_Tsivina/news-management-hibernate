package com.epam.news.dao.jpa;

import com.epam.news.dao.IUserDAO;
import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;
import com.epam.news.util.JPAEntityManager;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.JPADAOUtil.*;

public class JPAUserDAO implements IUserDAO {

    private EntityManagerFactory managerFactory;
    public JPAUserDAO(){
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }
    /**
     * Finds all users
     * @return list of users
     * @throws DAOException
     */
    @Override
    public List<User> findAll() throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        List<User> result = new ArrayList<>();
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT u FROM User u");
            result = query.getResultList();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Finds user by id
     * @param id
     * @return user
     * @throws DAOException
     */
    @Override
    public User findById(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        User result = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            result = manager.find(User.class, id);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Deletes user by id
     * @param id
     * @throws DAOException
     */
    @Override
    public void delete(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;

        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            User entity = manager.find(User.class,id);
            manager.remove(entity);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }
    /**
     * Creates user with given info
    * @param entity
    * @return inserted id
    * @throws DAOException
    */
    @Override
    public Long add(User entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            entity.setId(null);
            manager.persist(entity);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return entity.getId();
    }
    /**
     * Updates user info by id
     * @param entity
     * @throws DAOException
     */
    @Override
    public void update(User entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            User tempUser = manager.find(User.class,entity.getId());
            tempUser.setName(entity.getName());
            tempUser.setLogin(entity.getLogin());
            tempUser.setPassword(entity.getPassword());
            tempUser.setExpired(entity.getExpired());
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }
    /**
     * Finds user by login
     * @param login
     * @return user
     * @throws DAOException
     */
    @Override
    public User findByLogin(String login) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        User result = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT u FROM User u WHERE u.login = :login");
            query.setParameter("login",login);
            result = (User)query.getSingleResult();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Finds user role
     * @param id
     * @return user role
     * @throws DAOException
     */
    @Override
    public String findUserRole(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        String result = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT r.roleName FROM Role r WHERE r.userId = :id");
            query.setParameter("id",id);
            result = (String)query.getSingleResult();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }

}
