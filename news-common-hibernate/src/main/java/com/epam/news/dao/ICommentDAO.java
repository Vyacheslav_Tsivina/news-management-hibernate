package com.epam.news.dao;

import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;

public interface ICommentDAO {
    /**
     * Finds comment by id
     * @param id
     * @return comment if find is successful, returns null otherwise
     * @throws DAOException
     */
    Comment findById(Long id) throws DAOException;
    /**
     * Deletes comment by id
     * @param id of comment
     * @throws DAOException
     */
    void delete(Long id) throws DAOException;
    /**
     * Creates comment with given info
     * @param entity
     * @return id of inserted comment
     * @throws DAOException
     */
    Long add(Comment entity) throws DAOException;
    /**
     * Updates comment info by id
     * @param entity
     * @throws DAOException
     */
    void update(Comment entity) throws DAOException;
}
