package com.epam.news.dao.jpa;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.util.JPAEntityManager;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.JPADAOUtil.closeManager;

public class JPANewsDAO implements INewsDAO {
    private EntityManagerFactory managerFactory;

    public JPANewsDAO() {
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }
    /**
     * Finds news by id
     * @param id
     * @return
     * @throws DAOException
     */
    @Override
    public News findById(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;
        News result = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            result = manager.find(News.class, id);
            result.getComments().size();
            result.getTags().size();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Deletes news by id
     * @param id of news
     * @throws DAOException
     */
    @Override
    public void delete(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;

        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            News entity = manager.find(News.class,id);
            entity.setAuthor(null);
            entity.setTags(null);
            manager.remove(entity);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }
    /**
     * Creates news with given info
     * @param entity
     * @return id of inserted news
     * @throws DAOException
     */
    @Override
    public Long add(News entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            entity.setId(null);
           List<Tag> tags = new ArrayList<Tag>();
            if (entity.getTags()!= null) {
                for (Tag tag : entity.getTags()) {
                    tags.add(manager.find(Tag.class, tag.getId()));
                }
            }
            entity.setTags(tags);
            manager.persist(entity);//add
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return entity.getId();
    }
    /**
     * Updates news info by id
     * @param entity
     * @throws DAOException
     */
    @Override
    public void update(News entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            News tempNews = manager.find(News.class,entity.getId());
            tempNews.setTitle(entity.getTitle());
            tempNews.setShortText(entity.getShortText());
            tempNews.setFullText(entity.getFullText());
            tempNews.setCreationDate(entity.getCreationDate());
            tempNews.setModificationDate(entity.getModificationDate());

            tempNews.setAuthor(entity.getAuthor());
            tempNews.setComments(entity.getComments());
            tempNews.setTags(entity.getTags());
            manager.merge(tempNews);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }
    /**
     * Finds count of news by given tags, author, tags, page
     * @param filterVO
     * @return list of news
     * @throws DAOException
     */
    @Override
    public int countNewsByFilter(FilterVO filterVO) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        int result = 0;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            String jpql = buildJPQLFilter(filterVO);
            Query query = manager.createQuery(jpql);
            insertSearchCriteria(query,filterVO);

            result = query.getResultList().size();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Finds news number in list of filtered news
     * @param newsId
     * @return news number
     * @throws DAOException
     */
    @Override
    public Integer newsNumberFromFilter(Long newsId, FilterVO filterVO) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        List<News> newsList;
        Integer result = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            String jpql = buildJPQLFilter(filterVO);

            Query query = manager.createQuery(jpql);
            insertSearchCriteria(query,filterVO);

            newsList = query.getResultList();
            for(int i=0;i<newsList.size();i++) {
                if (newsId.equals(newsList.get(i).getId())){
                    result = i+1;
                    break;
                }
            }
            if (newsList.size() == 0) result = null;
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Finds news from filter by news number
     * @param newsNumber
     * @param filterVO
     * @return news
     * @throws DAOException
     */
    @Override
    public News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        News result = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            String jpql = buildJPQLFilter(filterVO);
            Query query = manager.createQuery(jpql);
            insertSearchCriteria(query,filterVO);


            query.setFirstResult(newsNumber-1);
            query.setMaxResults(1);
            result = (News)query.getSingleResult();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Returns list of news from filter on given page number.
     * Page numbers begin with 1. News ordered by comments number and modification date
     * @param newsOnPage how many news will be on page
     * @param pageNumber determine number of page which should be return
     * @param filterVO
     * @return list of news on given page
     */
    @Override
    public List<News> findNewsByFilters(int newsOnPage, int pageNumber , FilterVO filterVO) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        List<News> result = new ArrayList<News>();
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            String jpql = buildJPQLFilter(filterVO);

            Query query = manager.createQuery(jpql);
            insertSearchCriteria(query,filterVO);

            int startIndex;
            startIndex = (pageNumber -1) * newsOnPage;
            query.setFirstResult(startIndex);
            query.setMaxResults(newsOnPage);
            result = query.getResultList();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }


    private String buildJPQLFilter(FilterVO filterVO)
    {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT DISTINCT n FROM News n ");

            if (filterVO.getTagsSearch() != null) {
                jpql.append("JOIN n.tags as t ");
            }
            jpql.append("WHERE ");
            if (filterVO.getTagsSearch() != null) {
                jpql.append(" (t.id IN ?tagIds ) ");
            } else {
                jpql.append(" 1=1 ");
            }
            jpql.append(" AND");
            if (filterVO.getAuthorSearch() != null) {
                jpql.append(" (n.author.id = :authorId) ");
            }else{
                jpql.append(" 1=1 ");
            }

        jpql.append("ORDER BY size(n.comments) DESC , n.modificationDate DESC");
        return jpql.toString();
    }
    /**
     * Insert parameters from FilterVO into Query
     */
    private void insertSearchCriteria(Query query, FilterVO filterVO){

            if (filterVO.getTagsSearch() != null) {
                List<Long> tagIds = new ArrayList<Long>();
                for (Tag tag:filterVO.getTagsSearch()){
                    tagIds.add(tag.getId());
                }
                query.setParameter("tagIds", tagIds);
            }
            if (filterVO.getAuthorSearch() != null) {
                query.setParameter("authorId", filterVO.getAuthorSearch().getId());
            }

    }
}
