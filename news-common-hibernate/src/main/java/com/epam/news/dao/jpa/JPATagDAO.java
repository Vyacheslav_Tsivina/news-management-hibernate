package com.epam.news.dao.jpa;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.util.JPAEntityManager;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.JPADAOUtil.*;

public class JPATagDAO implements ITagDAO {
    private EntityManagerFactory managerFactory;
    public JPATagDAO(){
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }
    /**
     * Finds all tags
     * @return List of tags
     * @throws DAOException
     */
    @Override
    public List<Tag> findAll() throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        List<Tag> result = new ArrayList<Tag>();
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT t FROM Tag t ORDER BY t.name");
            result = query.getResultList();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Finds tag by id
     * @param id
     * @return tag if find is successful, returns null otherwise
     * @throws DAOException
     */
    @Override
    public Tag findById(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        Tag result = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            result = manager.find(Tag.class, id);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Deletes tag by id
     * @param id of tag
     * @throws DAOException
     */
    @Override
    public void delete(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;

        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Tag entity = manager.find(Tag.class,id);
            entity.setNews(null);
            manager.remove(entity);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }
    /**
     * Creates tag with given info
     * @param entity
     * @return id of inserted tag
     * @throws DAOException
     */
    @Override
    public Long add(Tag entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            entity.setId(null);
            manager.persist(entity);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return entity.getId();
    }
    /**
     * Updates tag info by id
     * @param entity
     * @throws DAOException
     */
    @Override
    public void update(Tag entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Tag tempTag = manager.find(Tag.class,entity.getId());
            tempTag.setName(entity.getName());
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }

}
