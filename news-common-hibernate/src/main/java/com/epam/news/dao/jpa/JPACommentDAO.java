package com.epam.news.dao.jpa;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.util.JPAEntityManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import static com.epam.news.util.JPADAOUtil.*;

public class JPACommentDAO implements ICommentDAO {
    private EntityManagerFactory managerFactory;

    public JPACommentDAO(){
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }
    /**
     * Finds comment by id
     * @param id
     * @return comment if find is successful, returns null otherwise
     * @throws DAOException
     */
    @Override
    public Comment findById(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;
        Comment result = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            result = manager.find(Comment.class, id);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }
    /**
     * Deletes comment by id
     * @param id of comment
     * @throws DAOException
     */
    @Override
    public void delete(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;

        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Comment entity = manager.find(Comment.class,id);
            entity.setNews(null);
            manager.remove(entity);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }
    /**
     * Creates comment with given info
     * @param entity
     * @return id of inserted comment
     * @throws DAOException
     */
    @Override
    public Long add(Comment entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            entity.setId(null);
            manager.persist(entity);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return entity.getId();
    }
    /**
     * Updates comment info by id
     * @param entity
     * @throws DAOException
     */
    @Override
    public void update(Comment entity) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Comment tempComment = manager.find(Comment.class,entity.getId());
            tempComment.setCommentText(entity.getCommentText());
            tempComment.setCreationDate(entity.getCreationDate());
            tempComment.setNews(entity.getNews());
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }
}
