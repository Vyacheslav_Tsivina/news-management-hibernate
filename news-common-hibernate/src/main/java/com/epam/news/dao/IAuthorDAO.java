package com.epam.news.dao;

import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;

import java.util.List;

public interface IAuthorDAO {
    /**
     * Finds all authors
     * @return List of authors
     * @throws DAOException
     */
    List<Author> findAll() throws DAOException;
    /**
     * Finds author by id
     * @param id
     * @throws DAOException
     */
    Author findById(Long id) throws DAOException;
    /**
     * Deletes author by id
     * @param id of author
     * @throws DAOException
     */
    void delete(Long id) throws DAOException;
    /**
     * Creates author with given info
     * @param entity
     * @return id of inserted author
     * @throws DAOException
     */
    Long add(Author entity) throws DAOException;
    /**
     * Updates author info by id
     * @param entity
     * @throws DAOException
     */
    void update(Author entity) throws DAOException;
}
