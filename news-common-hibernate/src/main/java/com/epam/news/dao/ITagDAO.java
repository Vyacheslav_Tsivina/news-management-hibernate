package com.epam.news.dao;

import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

import java.util.List;

public interface ITagDAO {
    /**
     * Finds all tags
     * @return List of tags
     * @throws DAOException
     */
    List<Tag> findAll() throws DAOException;
    /**
     * Finds tag by id
     * @param id
     * @return tag if find is successful, returns null otherwise
     * @throws DAOException
     */
    Tag findById(Long id) throws DAOException;
    /**
     * Deletes tag by id
     * @param id of tag
     * @throws DAOException
     */
    void delete(Long id) throws DAOException;
    /**
     * Creates tag with given info
     * @param entity
     * @return id of inserted tag
     * @throws DAOException
     */
    Long add(Tag entity) throws DAOException;
    /**
     * Updates tag info by id
     * @param entity
     * @throws DAOException
     */
    void update(Tag entity) throws DAOException;
}
