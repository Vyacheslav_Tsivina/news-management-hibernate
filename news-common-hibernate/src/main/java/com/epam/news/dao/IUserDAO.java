package com.epam.news.dao;

import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;

import java.util.List;

public interface IUserDAO {
    /**
     * Finds all users
     * @return list of users
     * @throws DAOException
     */
    List<User> findAll() throws DAOException;

    /**
     * Finds user by id
     * @param id
     * @return user
     * @throws DAOException
     */
    User findById(Long id) throws DAOException;

    /**
     * Deletes user by id
     * @param id
     * @throws DAOException
     */
    void delete(Long id) throws DAOException;

    /**
     * Creates user with given info
     * @param entity
     * @return inserted id
     * @throws DAOException
     */
    Long add(User entity) throws DAOException;

    /**
     * Updates user info by id
     * @param entity
     * @throws DAOException
     */
    void update(User entity) throws DAOException;

    /**
     * Finds user by login
     * @param login
     * @return user
     * @throws DAOException
     */
    User findByLogin(String login) throws DAOException;

    /**
     * Finds user role
     * @param id
     * @return user role
     * @throws DAOException
     */
    String findUserRole(Long id) throws DAOException;
}
