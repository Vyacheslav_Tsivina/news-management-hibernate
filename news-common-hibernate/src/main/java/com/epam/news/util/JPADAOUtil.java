package com.epam.news.util;

import com.epam.news.exception.DAOException;

import javax.persistence.EntityManager;

public class JPADAOUtil {
    public static void closeManager(EntityManager manager) throws DAOException {
        if ((manager != null) && manager.isOpen()) {
            try {
                manager.close();
            } catch (IllegalStateException e) {
                throw new DAOException(e);
            }
        }
    }
}
