package com.epam.news.dao.jpa;

import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import static org.junit.Assert.*;
import java.util.List;


/**
 * Test C.R.U.D operations from JPATagDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class JPATagDAOTest{
    private final static String FULL_DB_PATH="classpath:fullDB.xml";

    @Autowired
    JPATagDAO jpaTagDAO;

    /**
     * Test find by id
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void findByIdTest() throws DAOException{
        Tag actual = jpaTagDAO.findById(1L);

        Tag expected = new Tag(1L,"cinema");
        assertEquals(expected,actual);
    }

    /**
     * Test find all
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void findAllTest() throws DAOException{
        List<Tag> result = jpaTagDAO.findAll();
        assertEquals(23,result.size());
    }
    /**
     * Test delete
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void deleteTest() throws DAOException{
        jpaTagDAO.delete(21L);
        List<Tag> tags = jpaTagDAO.findAll();
        assertEquals(22,tags.size());
    }

    /**
     * Test add
     * @throws DAOException
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void addTest() throws DAOException{
        Tag tag = new Tag();
        tag.setName("qwerty");
        jpaTagDAO.add(tag);
    }
    /**
     * Test update
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void updateTest()throws DAOException{
        Tag expected = jpaTagDAO.findById(7L);
        expected.setName("qwerty");
        jpaTagDAO.update(expected);
        Tag actual = jpaTagDAO.findById(7L);
        assertEquals(expected,actual);
    }
}
