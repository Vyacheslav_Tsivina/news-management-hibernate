package com.epam.news.dao.jpa;

import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;

/**
 * Test C.R.U.D operations from JPAAuthorDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class JPAAuthorDAOTest{
    private final static String FULL_DB_PATH="classpath:fullDB.xml";

    @Autowired
    JPAAuthorDAO jpaAuthorDAO;

    /**
     * Test find by id
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindById() throws DAOException {
        Author expected = new Author(1L,"Ernest Lombardo",null);
        Author actual = jpaAuthorDAO.findById(1L);
        assertEquals(expected,actual);
    }
    /**
     * Test find all
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindAll() throws DAOException{
        List<Author> authorList = jpaAuthorDAO.findAll();
        assertEquals(20,authorList.size());
    }
    /**
     * Test add
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testAdd() throws DAOException{
        Author expected = new Author(1L,"qwerty", new Date());
        jpaAuthorDAO.add(expected);
        Author actual = jpaAuthorDAO.findById(expected.getId());
        assertEquals(expected,actual);
    }
    /**
     * Test update
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testUpdate() throws  DAOException{
        Author excepted = jpaAuthorDAO.findById(1L);
        excepted.setName("qwerty");
        excepted.setExpired(new Date());
        jpaAuthorDAO.update(excepted);
        Author actual = jpaAuthorDAO.findById(excepted.getId());
        assertEquals(excepted,actual);
    }
    /**
     * Test delete
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testDelete() throws  DAOException{
        Author author = new Author(null,"qwertt",null);

        jpaAuthorDAO.add(author);
        jpaAuthorDAO.delete(author.getId());
        List<Author> authorList = jpaAuthorDAO.findAll();
        assertEquals(20,authorList.size());
    }
}
