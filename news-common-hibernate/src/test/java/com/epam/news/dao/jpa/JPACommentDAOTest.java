package com.epam.news.dao.jpa;

import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;


import java.util.Date;
import static org.junit.Assert.*;

/**
 * Test C.R.U.D operations from JPACommentDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class JPACommentDAOTest{
    private final static String FULL_DB_PATH="classpath:fullDB.xml";

    @Autowired
    JPACommentDAO jpaCommentDAO;

    /**
     * Test find by id
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void findByIdTest() throws DAOException {
        Comment expected = new Comment(6L,"Its ok",null,null);
        Comment actual = jpaCommentDAO.findById(6L);
        actual.setCreationDate(null);
        assertEquals(expected,actual);
    }
    /**
     * Test add
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void addTest() throws DAOException{
        Comment expected = new Comment(1L,"qwerty", new Date(), null);
        jpaCommentDAO.add(expected);
        Comment actual = jpaCommentDAO.findById(expected.getId());
        assertEquals(expected,actual);
    }
    /**
     * Test update
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void updateTest() throws  DAOException{
        Comment expected = jpaCommentDAO.findById(1L);
        expected.setCommentText("asdfgh");
        expected.setCreationDate(new Date());
        jpaCommentDAO.update(expected);
        Comment actual = jpaCommentDAO.findById(1L);
        assertEquals(expected,actual);
    }
    /**
     * Test delete
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void deleteTest() throws DAOException{
        jpaCommentDAO.delete(1L);
        Comment comment = jpaCommentDAO.findById(1L);
        assertNull(comment);
    }
}
