package com.epam.news.dao.jpa;

import com.epam.news.entity.*;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;


import java.util.*;
import static org.junit.Assert.*;

/**
 * Test C.R.U.D operations from JPANewsDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class JPANewsDAOTest {
    private final static String FULL_DB_PATH="classpath:fullDB.xml";

    @Autowired
    JPANewsDAO jpaNewsDAO;
    @Autowired
    JPATagDAO jpaTagDAO;
    @Autowired
    JPAAuthorDAO jpaAuthorDAO;
    @Autowired
    JPACommentDAO jpaCommentDAO;
    private FilterVO filterVO = new FilterVO(null,null);

    /**
     * Test findNewsByFilters by author
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindNewsByAuthor() throws DAOException
    {
        filterVO.setAuthorSearch(new Author(1L,"",null));
        List<News> newsFilter = jpaNewsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(3, newsFilter.size());
    }
    /**
     * Test findNewsByFilters by tag/tags
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindNewsByTags() throws DAOException
    {
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(new Tag(21L,"NATO"));
        filterVO.setTagsSearch(tags);
        List<News> newsFilter = jpaNewsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(3, newsFilter.size());

        tags.add(new Tag(22L, "Europe"));
        filterVO.setTagsSearch(tags);
        newsFilter = jpaNewsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(3, newsFilter.size());
    }

    /**
     * Test findNewsByFilters by tags and author
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindNewsByAuthorTags() throws DAOException
    {
        Author author = new Author (1L,"",null);
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(new Tag(15L,"military"));

        filterVO.setTagsSearch(tags);
        filterVO.setAuthorSearch(author);

        List<News> newsFilter = jpaNewsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(2, newsFilter.size());

        tags.add(new Tag(14L,"protest"));
        newsFilter = jpaNewsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(2, newsFilter.size());
    }

    /**
     * Test findNewsByFilters without searchCriteria
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindNews() throws DAOException
    {
        List<News> newsFilter = jpaNewsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(9, newsFilter.size());
    }

    /**
     * Test delete
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void deleteTest() throws DAOException{

        jpaNewsDAO.delete(9L);

        assertEquals(8,jpaNewsDAO.countNewsByFilter(filterVO));
    }

    /**
     * Count news from filter test
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void countNewsFromFilter() throws DAOException{
        Author author = new Author (1L,"",null);
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(new Tag(15L,"military"));

        filterVO.setTagsSearch(tags);
        filterVO.setAuthorSearch(author);
        assertEquals(2, jpaNewsDAO.countNewsByFilter(filterVO));

        resetFilter();
        filterVO.setAuthorSearch(new Author(1L,"",null));

        assertEquals(3, jpaNewsDAO.countNewsByFilter(filterVO));
    }

    /**
     * News from filter by number test
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void newsFromFilterByNumberTest() throws DAOException{
        Author author = new Author (1L,"",null);
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(new Tag(15L,"military"));

        filterVO.setTagsSearch(tags);
        filterVO.setAuthorSearch(author);

        assertEquals((Long)2L,jpaNewsDAO.newsFromFilterByNumber(1,filterVO).getId() );
        assertEquals((Long)3L,jpaNewsDAO.newsFromFilterByNumber(2,filterVO).getId() );

    }

    /**
     * add, update test
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void addUpdateTest() throws DAOException{
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(jpaTagDAO.findById(1L));
        tags.add(jpaTagDAO.findById(2L));
        tags.add(jpaTagDAO.findById(3L));
        Author author = jpaAuthorDAO.findById(1L);
        News news = new News();

        news.setTitle("qwerty");
        news.setShortText("qwerty");
        news.setFullText("qwerty");
        news.setAuthor(author);
        news.setTags(tags);
        jpaNewsDAO.add(news);
        assertEquals(10,jpaNewsDAO.countNewsByFilter(filterVO));//test add

        Comment comment = new Comment(null,"qwerty",new Date(),jpaNewsDAO.findById(news.getId()));
        jpaCommentDAO.add(comment);
        comment = new Comment(null,"qwertyqwerty",new Date(),jpaNewsDAO.findById(news.getId()));
        jpaCommentDAO.add(comment);
        news = jpaNewsDAO.findById(news.getId());

        tags.add(jpaTagDAO.findById(4L));
        author = jpaAuthorDAO.findById(2L);
        news.setTags(tags);
        news.setAuthor(author);
        jpaNewsDAO.update(news);
        News newsActual = jpaNewsDAO.findById(news.getId());
        assertEquals(news.getTitle(),newsActual.getTitle());
        assertEquals(news.getAuthor(),newsActual.getAuthor());//test update
    }
    @Before
    public void resetFilter()
    {
        filterVO.setTagsSearch(null);
        filterVO.setAuthorSearch(null);
    }
}
