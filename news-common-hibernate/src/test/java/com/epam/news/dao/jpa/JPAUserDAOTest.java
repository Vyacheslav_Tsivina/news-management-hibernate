package com.epam.news.dao.jpa;

import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;


import java.util.Date;
import static org.junit.Assert.*;
/**
 * Test C.R.U.D operations from JPAUserDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class JPAUserDAOTest{
    private final static String FULL_DB_PATH="classpath:fullDB.xml";

    @Autowired
    JPAUserDAO jpaUserDAO;

    /**
     * Find by id test
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void findByIdTest() throws DAOException{
        User expected = new User(3L,"user3","user3","d033e22ae348aeb5660fc2140aec35850c4da997",null);
        User actual = jpaUserDAO.findById(3L);
        assertEquals(expected,actual);

    }
    /**
     * Add test
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void addTest() throws DAOException{
        User expected = new User(1L,"admin1","admin1","d033e22ae348aeb5660fc2140aec35850c4da997",new Date());
        jpaUserDAO.add(expected);
        User actual = jpaUserDAO.findById(expected.getId());
        assertEquals(expected,actual);
    }
    /**
     * Update test
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void updateTest() throws DAOException{
        User expected = new User(1L,"admin1","admin1","qwerty",new Date());
        jpaUserDAO.update(expected);
        User actual = jpaUserDAO.findById(expected.getId());
        assertEquals(expected,actual);
    }
    /**
     * Delete test
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void deleteTest() throws DAOException{
        jpaUserDAO.delete(1L);
        assertEquals(4,jpaUserDAO.findAll().size());
    }
    /**
     * Find by login
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void findByLoginTest() throws DAOException{
        User expected = new User(1L,"admin","admin","d033e22ae348aeb5660fc2140aec35850c4da997",null);
        User actual = jpaUserDAO.findByLogin(expected.getLogin());
        assertEquals(expected,actual);
    }
    /**
     * Find user role
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void findUserRoleTest() throws DAOException{
        String expected = "ROLE_ADMIN";
        String actual = jpaUserDAO.findUserRole(1L);
        assertEquals(expected,actual);
    }
}
