package com.epam.news.dao;

import org.junit.Test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Viachaslau_Tsyvina on 7/9/2015.
 */
public class Test1 {

    class MyThread1 implements Runnable{
        private A i;
        public MyThread1(A a){
            this.i = a;
        }
        @Override
        public void run() {
                A a = new A();
                a.a(2);
        }
    }

    class MyThread2 implements Runnable{
        private Integer i;
        public MyThread2(Integer i){
            this.i = i;
        }
        @Override
        public void run() {
            synchronized (i){
                System.out.println("2 "+i);
            }
        }
    }

    void change(int s){
        s++;
    }

    @Test
    public void test() throws InterruptedException {
        int s = 1;
        change(s);
        System.out.println(s);
    }
}
