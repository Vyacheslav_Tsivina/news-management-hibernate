package com.epam.news.dao;

import com.epam.news.dao.hibernate.HibernateAuthorDAO;
import com.epam.news.dao.hibernate.HibernateCommentDAO;
import com.epam.news.dao.hibernate.HibernateNewsDAO;
import com.epam.news.dao.hibernate.HibernateTagDAO;
import com.epam.news.entity.*;
import com.epam.news.exception.DAOException;
import com.epam.news.util.DataGenerator;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.util.*;

/**
 * Test script which generates 10 000 records in DB
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class DatabaseFillerTest  {
    private final int ROW_NUMBER = 1000;
    private static Logger logger= Logger.getLogger(DatabaseFillerTest.class);
    @Autowired
    private HibernateNewsDAO newsDAO;
    @Autowired
    private HibernateAuthorDAO authorDAO;
    @Autowired
    private HibernateCommentDAO commentDAO;
    @Autowired
    private HibernateTagDAO tagDAO;



    private long insertStartTime;
    private long insertEndTime;
    private long findStartTime;
    private long findEndTime;

    @Test
    public void bogus() throws CloneNotSupportedException {

    }
    //@Test
    public void testFindNewsByFilter() throws DAOException{
     /*   insertStartTime = System.nanoTime();
        logger.debug("Start author insert");
        fillAuthorTable();
        logger.debug("Start tag insert");
        fillTagTable();
        logger.debug("Start news insert");
        fillNewsTable();
        logger.debug("Start comment insert");
        fillCommentTable();
        insertEndTime = System.nanoTime();*/

        List<Tag> tagList = tagDAO.findAll();
        List<Tag> filterTagList = new ArrayList<Tag>();
        Random random = new Random();

        FilterVO filterVO = new FilterVO(new ArrayList<Tag>(),null);
        filterVO.getTagsSearch().add(tagList.get(random.nextInt(ROW_NUMBER)));
        findStartTime = System.nanoTime();
        List<News> newsList;
        logger.debug("Start find insert");
        for (int i=1; i<20; i++){
            filterVO.getTagsSearch().add(tagList.get(random.nextInt(ROW_NUMBER)));
            newsList = newsDAO.findNewsByFilters(ROW_NUMBER,1,filterVO);
        }

        findEndTime = System.nanoTime();

        logger.debug("Insert time = " + (double) (insertEndTime - insertStartTime) / 1000000000.0 + " find time = " + (double) (findEndTime - findStartTime) / 1000000000.0);

    }

    private void fillAuthorTable() throws DAOException {
        Set<String> nameSet = DataGenerator.generateNames(ROW_NUMBER);
        for (String name: nameSet){
            Author author = new Author();
            author.setName(name);
            authorDAO.add(author);
        }
    }

    private void fillTagTable() throws DAOException{
        Set<String> nameSet = DataGenerator.generateNames(ROW_NUMBER);
        for (String name: nameSet){
            Tag tag = new Tag();
            tag.setName(name);
            tagDAO.add(tag);
        }
    }

    private  void fillNewsTable() throws  DAOException{
        Set<String> stringSet = DataGenerator.generateNames(ROW_NUMBER);
        List<Author> authorList = authorDAO.findAll();
        List<Tag> tagList = tagDAO.findAll();
        Random random = new Random();

        for (String str: stringSet){
            News news = new News();
            news.setTitle(str);
            news.setShortText(str);
            news.setFullText(str);
            news.setCreationDate(new Date());
            news.setModificationDate(new Date());

            news.setAuthor(authorList.get(random.nextInt(ROW_NUMBER)));
            for (int i=0;i<5;i++) {
               news.getTags().add(tagList.get(random.nextInt(ROW_NUMBER)));
            }

            newsDAO.add(news);
        }
    }

    private void fillCommentTable() throws DAOException{
        Set<String> commentTextSet = DataGenerator.generateNames(ROW_NUMBER);
        FilterVO filterVO = new FilterVO(null,null);
        List<News> newsList = newsDAO.findNewsByFilters(ROW_NUMBER,1,filterVO);
        Random random = new Random();
        for (String commentText: commentTextSet){
            Comment comment = new Comment();
            comment.setCommentText(commentText);
            comment.setCreationDate(new Date());
            comment.setNews(newsList.get(random.nextInt(ROW_NUMBER)));

            commentDAO.add(comment);
        }
    }
}
