<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="content">
    <a href="/news-filter/${pageNumber}"><fmt:message key="link.back"/></a><br/>
    <div class="title-author-date">
        <div class="news-title">${news.title}</div>
        <div class="news-author-date">
            (<fmt:message key="main.message.by"/> ${news.author.name})
            <span class="news-modification-date"><ctg:dateFormat date="${news.modificationDate}" /></span>
        </div>
    <div class="news-full-text">
        ${news.fullText}
    </div >
    <div class="comments">
        <c:forEach var="comment" items="${news.comments}">
            <div>
                <span class="comment-date"> <ctg:dateFormat date="${comment.creationDate}" /></span>
                <div class="comment-text">${comment.commentText}</div>
            </div>
        </c:forEach>
        <form action="/add-comment">
            <textarea name="commentText" cols="40" rows="6"></textarea><br />
            <input type="hidden" name="newsId" value="${news.id}" />
            <input type="submit" value="<fmt:message key="single.button.add.comment" />" />
        </form>
    </div>
        <div>
            <c:if test="${prevId != null}"><a href="/single-news/${prevId}"><fmt:message key="single.link.previous"/> </a></c:if>
            <c:if test="${nextId != null}"><a href="/single-news/${nextId}" class="single-next-link"><fmt:message key="single.link.next"/> </a></c:if>
        </div>
</div>
