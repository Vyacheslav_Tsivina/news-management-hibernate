package com.epam.newsmanagement.controller;

import com.epam.news.entity.Author;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;

/**
 * Class provides actions:
 * - get news by filter ang page
 * - reset filter
 * - get single news by id
 * - add news
 * - delete news
 * - edit news
 */
@Controller
public class NewsController {

    @Autowired
    ITagService tagService;
    @Autowired
    IAuthorService authorService;
    @Autowired
    INewsService newsService;

    private static final int NEWS_ON_PAGE = 3;

    /**
     * Get news by filter and page
     */
    @Secured("ROLE_ADMIN")
    @RequestMapping(value=NEWS_FILTER+ NEWS_PAGE_NUMBER_PARAM)
    public ModelAndView newsFilter(HttpSession session,
                                   @RequestParam(value = "author", required = false) Long authorId,
                                   @RequestParam(value = "tags", required = false) List<Long> tagIds,
                                   @RequestParam(value = "fromFilter", required = false) Boolean fromFilter,
                                   @PathVariable Integer pageNumber) throws ServiceException {

        return newsFilterModel(session, authorId, tagIds, pageNumber, fromFilter);// general method for news by filter
    }

    /**
     * Get news by filter, without page number parameter
     */
    @Secured("ROLE_ADMIN")
    @RequestMapping(value=NEWS_FILTER)
    public ModelAndView newsFilter(HttpSession session,
                                   @RequestParam(value = "author", required = false) Long authorId,
                                   @RequestParam(value = "tags", required = false) List<Long> tagIds,
                                   @RequestParam(value = "fromFilter", required = false) Boolean fromFilter
    ) throws ServiceException {
        return newsFilterModel(session, authorId, tagIds, 1, fromFilter);// general method for news by filter
    }

    /**
     * Reset filter and return first page on news
     */
    @Secured("ROLE_ADMIN")
    @RequestMapping(value=RESET_FILTER)
    public ModelAndView resetFilter(HttpSession session) throws ServiceException {
        session.setAttribute("authorId",null);
        session.setAttribute("tagIds",null);
        return newsFilterModel(session, null, null, 1, null);
    }

    /**
     * Find single news by id
     */
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = SINGLE_NEWS)
    public ModelAndView singleNews(HttpSession session,
                                   @PathVariable Long newsId) throws ServiceException
    {
        FilterVO filterVO = getFilterFromSession(session,null,null);
        News news = newsService.findById(newsId);
        int newsNumber = newsService.newsNumberFromFilter(newsId,filterVO);
        Long prevId = null;
        Long nextId = null;
        if (newsNumber > 1){
            prevId = newsService.newsFromFilterByNumber(newsNumber-1,filterVO).getId();
        }
        if (newsNumber < newsService.countNewsByFilter(filterVO)){
            nextId = newsService.newsFromFilterByNumber(newsNumber+1,filterVO).getId();
        }

        session.setAttribute("news",news);
        session.setAttribute("prevId",prevId);
        session.setAttribute("nextId",nextId);
        session.setAttribute("pageNumber", (newsNumber - 1) / NEWS_ON_PAGE + 1);
        return new ModelAndView(NEWS_SINGLE_VIEW);
    }

    /**
     * Add news
     */
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = ADD_NEWS)
    public ModelAndView addNews(HttpSession session,
                                @RequestParam(value = "title", required = true) String title,
                                @RequestParam(value = "shortText", required = true) String shortText,
                                @RequestParam(value = "fullText", required = true) String fullText,
                                @RequestParam(value = "author", required = true) Long authorId,
                                @RequestParam(value = "tags", required = false) List<Long> tagIds,
                                @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam(value= "creationDate", required = true) Date creationDate)
            throws ServiceException
    {
        List<Tag> tags = null;
        if (tagIds != null) {//find tags by tagIds
            tags = new ArrayList<>();
            for (Long id : tagIds) {
                tags.add(tagService.findById(id));
            }
        }
        Author author = authorService.findById(authorId);

        News news = new News();//set all fields
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setCreationDate(creationDate);
        news.setModificationDate(creationDate);
        news.setAuthor(author);
        news.setTags(tags);
        newsService.add(news);

        session.setAttribute("authorId", null);//to avoid errors with old filter
        session.setAttribute("tagIds", null);
        session.setAttribute("news", news);
        session.setAttribute("prevNews", null);
        session.setAttribute("nextNews", null);
        session.setAttribute("pageNumber", 1);
        return new ModelAndView(REDIRECT+"single-news/"+news.getId());
    }

    /**
     * Delete news by ids
     */
    @Secured("ROLE_ADMIN")
    @RequestMapping(DELETE_NEWS)
    public ModelAndView deleteNews(HttpSession session,
                                   @RequestParam(value = "newsToDelete", required = false) List<Long> newsToDelete)
            throws ServiceException
    {
        prepareView(session);
        for (Long id : newsToDelete) {
            newsService.delete(id);
        }
        return new ModelAndView(REDIRECT+NEWS_FILTER);
    }

    /**
     * Edit news
     */
    @Secured("ROLE_ADMIN")
    @RequestMapping(EDIT_NEWS)
    public ModelAndView editNews(HttpSession session,
                                 @RequestParam(value = "newsId", required = true) Long newsId,
                                 @RequestParam(value = "title", required = true) String title,
                                 @RequestParam(value = "shortText", required = true) String shortText,
                                 @RequestParam(value = "fullText", required = true) String fullText,
                                 @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam(value = "creationDate", required = true) Date creationDate,
                                 @RequestParam(value = "authorId", required = true) Long authorId,
                                 @RequestParam(value = "tags", required = false) List<Long> tagIds,
                                 @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam(value= "modificationDate", required = true) Date modificationDate)
            throws ServiceException
    {

        List<Tag> tags = null;
        if (tagIds != null) {//find tags by tagIds
            tags = new ArrayList<>();
            for (Long id : tagIds) {
                tags.add(tagService.findById(id));
            }
        }
        Author author = authorService.findById(authorId);
        News news = newsService.findById(newsId);
        news.setTitle(title);// set new fields
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setCreationDate(creationDate);
        news.setModificationDate(modificationDate);
        news.setAuthor(author);
        news.setTags(tags);

        try {
            session.setAttribute("optimisticLockException",false);
            newsService.update(news);
        }catch(ServiceException e){
            session.setAttribute("optimisticLockException",true);//in case of optimistic lock set bool attributes
            return new ModelAndView(EDIT_NEWS_VIEW);//back to edit page
        }
        return new ModelAndView(REDIRECT+"single-news/"+newsId);
    }
    /**
     * Exception handler
     */
    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler()
    {
        return new ModelAndView(ERROR);
    }

    /**
     * Set attributes for view
     */
    private void prepareView(HttpSession session) throws ServiceException {
        List<Author> authorList = authorService.findAll();
        List<Tag> tagList = tagService.findAll();
        session.setAttribute("tagList",tagList);
        session.setAttribute("authorList",authorList);
    }

    /**
     * General method for newsFilter
     */
    private ModelAndView newsFilterModel(HttpSession session,
                                         Long authorId,
                                         List<Long> tagIds,
                                         Integer pageNumber,
                                         Boolean fromFilter) throws ServiceException {
        if (fromFilter != null){//if request came not from filter button
            session.setAttribute("authorId",authorId);
            session.setAttribute("tagIds", tagIds);
        }
        if (fromFilter != null && tagIds == null) {//if we have 0 tags from filter
            session.setAttribute("tagIds",null);
        }
        prepareView(session);
        FilterVO filterVO = getFilterFromSession(session, authorId, tagIds);

        List<News> newsList;
        int newsCount = newsService.countNewsByFilter(filterVO);
        if (pageNumber == null) {//if use filter
            newsList = newsService.findNewsByFilters(NEWS_ON_PAGE, pageNumber, filterVO);
        } else {//if change page
            newsList = newsService.findNewsByFilters(NEWS_ON_PAGE, pageNumber, filterVO);
        }

        session.setAttribute("pageCount",newsCount / NEWS_ON_PAGE + (newsCount%NEWS_ON_PAGE==0?0:1));
        if (pageNumber == null){
            session.setAttribute("pageNumber",1);
        }else{
            session.setAttribute("pageNumber",pageNumber);
        }
        session.setAttribute("newsOnPage",NEWS_ON_PAGE);
        session.setAttribute("newsList",newsList);

        return new ModelAndView(NEWS_FILTER_VIEW);
    }

    /**
     * Build filterVO by author and tags ids
     */
    private FilterVO getFilterFromSession(HttpSession session,
                                          Long authorId,
                                          List<Long> tagIds) throws ServiceException
    {

        if (tagIds == null) tagIds = (List<Long>) session.getAttribute("tagIds");
        if (authorId == null) authorId = (Long) session.getAttribute("authorId");
        List<Tag> tags = null;
        if (tagIds != null) {
            tags = new ArrayList<>();
            for (Long id : tagIds) {
                tags.add(tagService.findById(id));
            }
        }
        Author author = authorId == null ? null : authorService.findById(authorId);
        return new FilterVO(tags,author);
    }
}
