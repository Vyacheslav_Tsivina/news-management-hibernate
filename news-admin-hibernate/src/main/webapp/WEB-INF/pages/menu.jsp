<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="admin-menu">
    <ul>
        <li>
            <a href="<spring:url value="/news-filter"/>" ><fmt:message key="menu.link.news-list" /> </a>
        </li>
        <li>
            <a href="<spring:url value="/add-news-view"/>" ><fmt:message key="menu.link.add.news"/> </a>
        </li>
        <li>
            <a href="<spring:url value="/add-author-view"/>" ><fmt:message key="menu.link.add.author" /></a>
        </li>
        <li>
            <a href="<spring:url value="/add-tag-view"/>" ><fmt:message key="menu.link.add.tags" /> </a>
        </li>
    </ul>
</div>
