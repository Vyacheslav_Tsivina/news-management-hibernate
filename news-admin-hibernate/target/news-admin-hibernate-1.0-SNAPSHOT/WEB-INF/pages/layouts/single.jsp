<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<div id="content">
    <a href="<spring:url value="/news-filter/${pageNumber}"/>"><fmt:message key="link.back"/></a><br/>
    <div class="title-author-date" id="single-title">
        <div class="news-title">${news.title}</div>
        <div class="news-author-date">
            (<fmt:message key="main.message.by"/> ${news.author.name})
            <span class="news-modification-date"><ctg:dateFormat date="${news.modificationDate}"/></span>
        </div></div>
    <div class="news-full-text">
        ${news.fullText}
    </div >
    <div class="comments">
        <c:forEach var="comment" items="${news.comments}">
            <form action="/news-admin/delete-comment" method="post">
                <div>
                    <span class="comment-date">  <ctg:dateFormat date="${comment.creationDate}" /></span>
                    <div class="comment-text">${comment.commentText}
                        <input class="delete-comment-button" type="submit" value="x"/>
                        <input type="hidden" name="newsId" value="${news.id}" />
                        <input type="hidden" name="commentId" value="${comment.id}" />
                    </div>
                </div>
            </form>
        </c:forEach>
        <form action="/news-admin/add-comment" method="post">
            <textarea name="commentText" cols="40" rows="6" required></textarea><br />
            <input type="hidden" name="newsId" value="${news.id}" />
            <input type="submit" value="<fmt:message key="single.button.add.comment" />" />
        </form>
    </div>
    <div>
        <c:if test="${prevId != null}"><a href="<spring:url value="/single-news/${prevId}"/>"><fmt:message key="single.link.previous"/> </a></c:if>
        <c:if test="${nextId != null}"><a href="<spring:url value="/single-news/${nextId}"/>" class="single-next-link"><fmt:message key="single.link.next"/> </a></c:if>
    </div>
</div>
