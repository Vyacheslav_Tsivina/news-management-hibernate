<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<a href="<spring:url value="/news-filter"/>">test</a>
<div class="login-content"  >
    <form action="/news-admin/spring_security_check" method="post">
        <h2 class="form-signin-heading"><fmt:message key="login.message.login" /> </h2>
        <input type="text"  name="username"  required autofocus ><br/>
        <input type="password"  name="password"  required ><br />
        <button type="submit"><fmt:message key="login.button.login" /></button>
    </form>
</div>